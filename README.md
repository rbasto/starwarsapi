### StarWarsAPI ###

Requisitos:

- A API deve ser REST
- Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente:
Nome
Clima
Terreno
- Para cada planeta também devemos ter a quantidade de aparições em filmes, que podem ser obtidas pela API pública do Star Wars:  https://swapi.co/

Funcionalidades desejadas:

- Adicionar um planeta (com nome, clima e terreno)
- Listar planetas
- Buscar por nome
- Buscar por ID
- Remover planeta

-----------------------------------------------------------------------------------------------------------------------
*** OBS: É necessário o MongoDB rodando no local da aplicação em sua porta padrão (27017)

URLs da API

Listar todos os planetas:
- HTTP GET http://localhost:8080/planetas/

Buscar planeta pelo nome:
- HTTP GET http://localhost:8080/planetas/?nome=<nome>

Buscar planeta pelo id:
- HTTP GET http://localhost:8080/planetas/<id>

Adicionar um planeta:
- HTTP PUT http://localhost:8080/planetas/

Remover um planeta:
- HTTP DELETE http://localhost:8080/planetas/<id>

ESQUEMA JSON DO OBJETO PLANETA:
    {
        "id": "",
        "nome": "",
        "clima": "",
        "terreno": "",
        "quantidadeAparicoes":
    }

