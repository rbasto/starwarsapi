package br.com.b2w.starwarsapi.repository;

import br.com.b2w.starwarsapi.entity.Planeta;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PlanetaRepository extends MongoRepository<Planeta, String> {

    public Optional<Planeta> findByNome(String nome);

}
