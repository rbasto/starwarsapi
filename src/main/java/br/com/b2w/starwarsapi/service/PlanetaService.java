package br.com.b2w.starwarsapi.service;


import br.com.b2w.starwarsapi.entity.Planeta;
import br.com.b2w.starwarsapi.model.PlanetaModel;
import br.com.b2w.starwarsapi.repository.PlanetaRepository;
import br.com.b2w.starwarsapi.swapi.SwApiResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlanetaService {

    private static final String swapiPlanetsURL = "https://swapi.co/api/planets/?search={0}";

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private PlanetaRepository planetaRepository;

    public List<PlanetaModel> listarPlanetas(){

        List<Planeta> planetas = planetaRepository.findAll();

        List<PlanetaModel> lista = new ArrayList<>();

        for(Planeta planeta: planetas){
            PlanetaModel model = new PlanetaModel();
            BeanUtils.copyProperties(planeta, model);
            model.setQuantidadeAparicoes(buscarQuantidadeAparicoesPlaneta(model.getNome()));
            lista.add(model);
        }

        return lista;
    }

    public PlanetaModel buscarPorId(String id){
        PlanetaModel model = new PlanetaModel();

        Optional<Planeta> planeta = planetaRepository.findById(id);

        if(planeta.isPresent()){
            BeanUtils.copyProperties(planeta.get(), model);
            model.setQuantidadeAparicoes(buscarQuantidadeAparicoesPlaneta(model.getNome()));
        }else{
            throw new EntityNotFoundException();
        }

        return model;
    }


    public PlanetaModel buscarPorNome(String nome){
        PlanetaModel model = new PlanetaModel();

        Optional<Planeta> planeta = planetaRepository.findByNome(nome);

        if(planeta.isPresent()){
            BeanUtils.copyProperties(planeta.get(), model);
            model.setQuantidadeAparicoes(buscarQuantidadeAparicoesPlaneta(model.getNome()));
        }else{
            throw new EntityNotFoundException();
        }

        return model;
    }

    public PlanetaModel incluirPlaneta(PlanetaModel model){
        Planeta planeta = new Planeta();
        BeanUtils.copyProperties(model, planeta);

        planeta = planetaRepository.save(planeta);

        BeanUtils.copyProperties(planeta, model);

        return  model;
    }

    public void removerPlaneta(String id){
        planetaRepository.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    public Integer buscarQuantidadeAparicoesPlaneta(String nome){

        Integer quant = 0;

        SwApiResult result = restTemplate.getForObject(MessageFormat.format(swapiPlanetsURL, nome), SwApiResult.class);

        if(result.getCount() > 0){
            quant = result.getResults().get(0).getFilms().size();
        }

        return quant;
    }
}


