package br.com.b2w.starwarsapi.swapi;

import java.util.List;

public class SwApiResult {

    private Integer count;
    private String next;
    private String previous;
    private List<SwApiPlanet> results;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<SwApiPlanet> getResults() {
        return results;
    }

    public void setResults(List<SwApiPlanet> results) {
        this.results = results;
    }
}
