package br.com.b2w.starwarsapi;

import br.com.b2w.starwarsapi.repository.PlanetaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class StarwarsapiApplication {

	@Autowired
	ObjectMapper jsonMapper;

	@Autowired
	PlanetaRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(StarwarsapiApplication.class, args);
	}

	@Bean
	public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {

		Resource sourceData = new ClassPathResource("data.json");
		Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();

		//Se o banco estiver vazio, popula com os dados iniciais.
		if(repository.count() <= 0) {
			// Set a custom ObjectMapper if Jackson customization is needed
			factory.setMapper(jsonMapper);
			factory.setResources(new Resource[]{sourceData});
		}
		return factory;
	}

}

@Configuration
class RestTemplateConfig {

	@Bean
	public RestTemplate restTemplate() {
		CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);

		return new RestTemplate(requestFactory);
	}
}


