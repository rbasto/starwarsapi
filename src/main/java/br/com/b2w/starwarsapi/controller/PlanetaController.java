package br.com.b2w.starwarsapi.controller;

import br.com.b2w.starwarsapi.model.PlanetaModel;
import br.com.b2w.starwarsapi.service.PlanetaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Map;

@RestController
public class PlanetaController {
    private final static Logger log = LoggerFactory.getLogger(PlanetaController.class);

    @Autowired
    private PlanetaService planetaService;

    @GetMapping(value = "/planetas")
    public ResponseEntity listarPlanetas(@RequestParam Map<String,String> params){
        try{
            if(params.containsKey("nome")) {
                return ResponseEntity.ok(planetaService.buscarPorNome(params.get("nome")));
            }
            return ResponseEntity.ok(planetaService.listarPlanetas());
        }catch (EntityNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (Exception e){
            log.error("Erro em listarPlanetas()", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @GetMapping(value = "/planetas/{id}")
    public ResponseEntity obterPlaneta(@PathVariable String id ){
        try{
            PlanetaModel model = planetaService.buscarPorId(id);
            return ResponseEntity.ok(model);
        }catch (EntityNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (Exception e){
            log.error("Erro em obterPlaneta(id)", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @DeleteMapping(value = "/planetas/{id}")
    public ResponseEntity removerPlaneta(@PathVariable String id ){
        try{
            planetaService.removerPlaneta(id);
        }catch (Exception e){
            log.error("Erro em removerPlaneta(id)", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok(null);
    }

    @PutMapping(value = "/planetas")
    public ResponseEntity incluirPlaneta(@RequestBody PlanetaModel model ){
        try {
            PlanetaModel modelCriado = planetaService.incluirPlaneta(model);
        }catch (Exception e){
            log.error("Erro em incluirPlaneta(model)", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(model);
    }


}
